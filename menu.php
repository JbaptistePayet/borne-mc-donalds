<html>

<link rel="stylesheet" type="text/css" href="styleMacDo.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<style>
  body {
    background-color: whitesmoke;
  }
</style>


<?php require_once("requetes.php");

$menus = getMenus();
$boissons = getBoissons();
$items = getItems();

?>



<table id="tabmenu">
  <tr>
    <td width=1520>
      <img src="../borne-mc-donalds/Images/CSS_logoVert.png"/ width=80 heigh=50>
    </td>
  </tr>
</table>
<p>
  <img src="../borne-mc-donalds/Images/CSS_banniere4.jpg"/ width=1520 height=450>
</p>
<p class="itemsTitre"><b> Nos burgers</b> </p>

<input type="button" value=" 🡰 Retour aux items" id="retour" />

<table class="menuTab">
  <tr>
    <td>
      <img src="../borne-mc-donalds/Images/burger-bigTasty.png" />
    </td>
    <td>
      <img src="../borne-mc-donalds/Images/burger-chickenBigTasty.png" />
    </td>
    <td>
      <img src="../borne-mc-donalds/Images/burger-CBO.png" />
    </td>
  </tr>

  <tr>
    <?php for ($i = 0; $i < 3; $i++) {
    ?>
      <td class="produit"> <?php echo (strval($menus[$i]["Nom"]));
                          } ?></td>
  </tr>
  <tr>
    <?php for ($i = 0; $i < 3; $i++) {
    ?>
      <td class="prix"> <?php echo (doubleval($menus[$i]["Prix"]) . ' €');
                      } ?></td>
  </tr>

</table>

<p class="itemsTitre"><b>Accompagnements</b> </p>
<table class="menuTab">
  <tr>
    <td>
      <img src="../borne-mc-donalds/Images/menu-frites.png" />
    </td>
    <td>
      <img src="../borne-mc-donalds/Images/menu-potatoes.png" />
    </td>
  </tr>
  <tr>
    <?php for ($i = 6; $i < 8; $i++) {
    ?>
      <td class="produit"> <?php echo (strval($items[$i]["Nom"]));
                          } ?></td>
  </tr>

</table>

<p class="itemsTitre"><b> Boissons</b> </p>
<table class="menuTab">
  <tr>
    <td>
      <img src="../borne-mc-donalds/Images/boisson-coca-cola.png" />
    </td>
    <td>
      <img src="../borne-mc-donalds/Images/boisson-ice-tea.png" />
    </td>
  </tr>
  <tr>
    <?php for ($i = 0; $i < 2; $i++) {
    ?>
      <td class="produit"> <?php echo (strval($boissons[$i]["Nom"]));
                          } ?></td>
  </tr>

</table>

<input type="button" value="Ajouter ce menu à la commande ✓" id="ajouterCommande" />

<p id="récap"> Votre commande : </p>
<table id="commande">
  <tr>
    <td class="itemsLigne">
      Items command&eacute;s
    <td class="itemsLigne">
    </td>
    Montant
    </td>
  </tr>
  <tr id="rempliAuto">

  </tr>
  <tr>
    <td class="itemsLigne">
      Total
    </td>
    <td class="itemsLigne" id="prixTotal">
      0
    </td>
  </tr>
</table>
<table id="pasPayer">
  <tr>
    <td id="boutonPasPayer">
      BOUTON PAYER INDISPONIBLE
    </td>
  </tr>
</table>


</html>


<script>
  commande = window.localStorage.getItem("commande");
  total = window.localStorage.getItem("total");
  total = parseFloat(total);
  idItems = window.localStorage.getItem("idItems");
  $('#rempliAuto').append(commande);
  $('#prixTotal').text(total);



  $("#retour").click(function() {
    window.location.href = 'http://localhost/borne-mc-donalds/items.php';
  });

  $("#ajouterCommande").click(function() {

    var param = {
      'idItems': idItems,
      'typeItems': "menus",
    };

    $.post('ajax.php', param, function(resultat) {
      itemsEtPrix = resultat;
      alert(itemsEtPrix[0] + " vient d'être ajouté à votre commande");
      $('#rempliAuto').append("<tr>" + "<td>" + itemsEtPrix[0] + "</td>" + "<td>" + itemsEtPrix[1] + "</td>" + "</tr>");
      total = total + parseFloat(itemsEtPrix[1]);
      window.localStorage.setItem("commande", $("#rempliAuto").html());
      window.localStorage.setItem("total", total);
      window.location.href = 'http://localhost/borne-mc-donalds/items.php';
    }, "json");
    $.post('ajax2.php', param, function(resultat) {}, "json");
  });
</script>