<?php

require_once("requetes.php");
$idItems = $_POST['idItems'];
$typeItems = $_POST['typeItems'];

function ajouter($idItems, $typeItems)
{
    if ($typeItems == 'burgers') {
        $burgers = getBurgers();
        $nom = strval($burgers[$idItems]["Nom"]);
        $prix = doubleval($burgers[$idItems]["Prix"]);
        $resultat = array($nom, $prix);
        return $resultat;
    }
    if ($typeItems == 'menus') {
        $menus = getMenus();
        $nom = strval($menus[$idItems]["Nom"]);
        $prix = doubleval($menus[$idItems]["Prix"]);
        $resultat = array($nom, $prix);
        return $resultat;
    }
    if ($typeItems == 'boissons') {
        $boissons = getBoissons();
        $nom = strval($boissons[$idItems]["Nom"]);
        $prix = doubleval($boissons[$idItems]["Prix"]);
        $resultat = array($nom, $prix);
        return $resultat;
    }
    if ($typeItems == 'items') {
        $items = getItems();
        $nom = strval($items[$idItems]["Nom"]);
        $prix = doubleval($items[$idItems]["Prix"]);
        $resultat = array($nom, $prix);
        return $resultat;
    }
    if ($typeItems == 'desserts') {
        $desserts = getDesserts();
        $nom = strval($desserts[$idItems]["Nom"]);
        $prix = doubleval($desserts[$idItems]["Prix"]);
        $resultat = array($nom, $prix);
        return $resultat;
    }
}

$resultat = ajouter($idItems, $typeItems);
echo json_encode($resultat);
