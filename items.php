<html>
<link rel="stylesheet" type="text/css" href="styleMacDo.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<style>
    body {
        background-color: whitesmoke;
    }
</style>




<?php
session_start();
require_once("requetes.php");

if ($isTouch = isset($_POST['placeOuEmporter'])) {
    $placeOuEmporter = $_POST['placeOuEmporter'];
    addLieu($placeOuEmporter);
}

$burgers = getBurgers();
$boissons = getBoissons();
$desserts = getDesserts();
$items = getItems();

?>
<p>
    <img src="../borne-mc-donalds/Images/CSS_banniere2.jpg"/ width=1520 height=400>
</p>
<p class="itemsTitre"><b> Nos burgers</b> </p>

<table class="itemsTab">
    <tr>
        <td>
            <input type="image" class="imageItems" idItems="0" typeItems="burgers" src="../borne-mc-donalds/Images/burger-bigTasty.png" />
        </td>

        <td>
            <input type="image" class="imageItems" idItems="1" typeItems="burgers" src="../borne-mc-donalds/Images/burger-chickenBigTasty.png" />

        </td>

        <td>
            <input type="image" class="imageItems" idItems="2" typeItems="burgers" src="../borne-mc-donalds/Images/burger-CBO.png" />
        </td>
    </tr>
    <tr>
        <?php for ($i = 0; $i < 3; $i++) {
        ?>
            <td class="produit"> <?php echo (strval($burgers[$i]["Nom"]));
                                } ?></td>
    </tr>
    <td>
        <input type="button" class="menuBurgers" idItems="0" typeItems="menus" value="Prendre en menu" />
    </td>
    <td>
        <input type="button" class="menuBurgers" idItems="1" typeItems="menus" value="Prendre en menu" />
    </td>
    <td>
        <input type="button" class="menuBurgers" idItems="2" typeItems="menus" value="Prendre en menu" />
    </td>
    <tr>

    </tr>
    <tr>
        <?php for ($i = 0; $i < 3; $i++) {
        ?>
            <td class="prix"> <?php echo (doubleval($burgers[$i]["Prix"]) . ' €');
                            } ?></td>
    </tr>

</table>

<p class="itemsTitre"> <b>P'tite faim </b> </p>
<table class="itemsTab">

    <tr>
        <td>
            <input type="image" class="imageItems" idItems="0" typeItems="items" src="../borne-mc-donalds/Images/items-MuffinEggAndBacon.png" />

        </td>

        <td>
            <input type="image" class="imageItems" idItems="1" typeItems="items" src="../borne-mc-donalds/Images/items-20-Chicken-MacNuggets.png" />

        </td>

        <td>
            <input type="image" class="imageItems" idItems="2" typeItems="items" src="../borne-mc-donalds/Images/items-boite-Nuggets.png" />

        </td>
        <td>
            <input type="image" class="imageItems" idItems="3" typeItems="items" src="../borne-mc-donalds/Images/items-croque-Macdo.png" />

        </td>

    </tr>
    <tr>
        <?php for ($i = 0; $i < 4; $i++) {
        ?>
            <td class="produit"> <?php echo (strval($items[$i]["Nom"]));
                                } ?></td>
    </tr>
    <tr>
        <?php for ($i = 0; $i < 4; $i++) {
        ?>
            <td class="prix"> <?php echo (doubleval($items[$i]["Prix"]) . ' €');
                            } ?></td>
    </tr>
    <tr>

        <td>
            <input type="image" class="imageItems" idItems="4" typeItems="items" src="../borne-mc-donalds/Images/items-hamburger.png" />
        </td>
        <td>
            <input type="image" class="imageItems" idItems="5" typeItems="items" src="../borne-mc-donalds/Images/items-petit-ranch.png" />
        </td>

        <td>
            <input type="image" class="imageItems" idItems="6" typeItems="items" src="../borne-mc-donalds/Images/menu-frites.png" />

        </td>
        <td>
            <input type="image" class="imageItems" idItems="7" typeItems="items" src="../borne-mc-donalds/Images/menu-potatoes.png" />

        </td>

    </tr>
    <tr>
        <?php for ($i = 4; $i < 8; $i++) {
        ?>
            <td class="produit"> <?php echo (strval($items[$i]["Nom"]));
                                } ?></td>
    </tr>
    <tr>
        <?php for ($i = 4; $i < 8; $i++) {
        ?>
            <td class="prix"> <?php echo (doubleval($items[$i]["Prix"]) . ' €');
                            } ?></td>
    </tr>
</table>

<p class="itemsTitre"> <b>Nos boissons</b> </p>

<table class="itemsTab">
    <tr>
        <td>
            <input type="image" class="imageItems" idItems="0" typeItems="boissons" src="../borne-mc-donalds/Images/boisson-coca-cola.png" />
        </td>
        <td>
            <input type="image" class="imageItems" idItems="1" typeItems="boissons" src="../borne-mc-donalds/Images/boisson-ice-tea.png" />
        </td>
    </tr>
    <tr>
        <?php for ($i = 0; $i < 2; $i++) {
        ?>
            <td class="produit"> <?php echo (strval($boissons[$i]["Nom"]));
                                } ?></td>
    </tr>
    <tr>
        <?php for ($i = 0; $i < 2; $i++) {
        ?>
            <td class="prix"> <?php echo (doubleval($boissons[$i]["Prix"]) . ' €');
                            } ?></td>
    </tr>
</table>

<p class="itemsTitre"><b> Desserts</b> </p>

<table class="itemsTab">
    <tr>
        <td>
            <input type="image" class="imageItems" idItems="0" typeItems="desserts" src="../borne-mc-donalds/Images/dessert-mcflurry.png"/ width=200 height=200 />
        </td>
        <td>
            <input type="image" class="imageItems" idItems="1" typeItems="desserts" src="../borne-mc-donalds/Images/dessert-sunday.png"/ width=220 height=220 />
        </td>
    </tr>
    <tr>
        <?php for ($i = 0; $i < 2; $i++) {
        ?>
            <td class="produit"> <?php echo (strval($desserts[$i]["Nom"]));
                                } ?></td>
    </tr>
    <tr>
        <?php for ($i = 0; $i < 2; $i++) {
        ?>
            <td class="prix"> <?php echo (doubleval($desserts[$i]["Prix"]) . ' €');
                            } ?></td>
    </tr>
</table>

<p id="récap"> Votre commande : </p>

<table id="commande">
    <tr>
        <td class="itemsLigne">
            Items command&eacute;s
        </td>
        <td class="itemsLigne">
            Montant
        </td>
    </tr>
    <tr id="rempliAuto">

    </tr>
    <tr>
        <td class="itemsLigne">
            Total
        </td>
        <td class="itemsLigne" id=prixTotal>
            0
        </td>
    </tr>
</table>
<table id="payer">
    <tr>
        <td>
            <input type="submit" value="PAYER" id="boutonPayer" />
        </td>
    </tr>
</table>

<script type="text/javascript">
    commande = window.localStorage.getItem("commande");
    total = window.localStorage.getItem("total");

    if (total != null) {
        total = parseFloat(total);
    }
    $('#rempliAuto').append(commande);
    $('#prixTotal').text(total);

    $(function() {

        $(".imageItems").click(function() {

            var param = {
                'idItems': $(this).attr('idItems'),
                'typeItems': $(this).attr('typeItems'),
            };

            $.post('ajax.php', param, function(resultat) {
                itemsEtPrix = resultat;
                alert(itemsEtPrix[0] + " vient d'être ajouté à votre commande");
                $('#rempliAuto').append("<tr>" + "<td>" + itemsEtPrix[0] + "</td>" + "<td>" + itemsEtPrix[1] + "</td>" + "</tr>");
                total = total + parseFloat(itemsEtPrix[1]);
                $('#prixTotal').text(total);
            }, "json");
            $.post('ajax2.php', param, function(resultat) {}, "json");
        });


    });

    $(".menuBurgers").click(function() {
        window.localStorage.setItem("commande", $("#rempliAuto").html());
        if (total == null) {
            total = 0;
        }
        window.localStorage.setItem("total", total);
        window.localStorage.setItem("idItems", $(this).attr('idItems'));
        window.location.href = 'http://localhost/borne-mc-donalds/menu.php';
    });


    $("#boutonPayer").click(function() {
        window.localStorage.setItem("commande", $("#rempliAuto").html());
        window.localStorage.setItem("total", total);
        window.location.href = 'http://localhost/borne-mc-donalds/commande.php';
    });
</script>



</html>